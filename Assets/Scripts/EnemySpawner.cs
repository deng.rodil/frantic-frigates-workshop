﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{
    public Transform Player;
    public GameObject EnemyPrefab;
    public int MaxSpawns = 10;
    public float SpawnRate = 3.0f;
    public float DistanceMin = 5.0f;
    public float DistanceMax = 7.0f;

    private float spawnTimer;

    private void Update()
    {
        // Spawn every interval
        spawnTimer += Time.deltaTime;
        if (spawnTimer > 1.0f / SpawnRate)
        {
            Spawn();
            spawnTimer = 0;
        }
    }

    private void Spawn()
    {
        // Check if player is still alive
        if (!Player) return;

        // Check spawn limit
        if (GameObject.FindGameObjectsWithTag("Enemy").Length >= MaxSpawns) return;

        // Spawn the enemy
        GameObject enemy = Instantiate(EnemyPrefab);

        // Randomize position
        float distance = Random.Range(DistanceMin, DistanceMax);
        float angle = Random.Range(-Mathf.PI, Mathf.PI);
        Vector3 pos = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)) * distance;
        pos.z = 0;
        enemy.transform.position = pos + Player.position;
    }
}
