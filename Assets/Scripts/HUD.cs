﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public GameObject Player;
    public Text HpText;
    public Text ScoreText;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Only update HP if the player is still alive
        if (Player) HpText.text = "HP: " + Player.GetComponent<Health>().Value.ToString();

        // Display score
        float percentScore = (float)GameManager.Instance.Score / GameManager.Instance.RequiredScore;
        ScoreText.text = "Score: " + GameManager.Instance.Score.ToString() + " (" + percentScore.ToString("P0") +")";
    }
}
