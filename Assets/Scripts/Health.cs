﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public int Value;
    public int KillScore;

    public void TakeDamage(int damage)
    {
        Value -= damage;
        if (Value < 0) Value = 0;

        // Check for death
        if (Value == 0)
        {
            // Increment score if this is an enemy
            if (tag == "Enemy")
            {
                GameManager.Instance.Score += KillScore;
            }

            Destroy(gameObject);
        }
    }
}
