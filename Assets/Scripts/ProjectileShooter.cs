﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    public GameObject Projectile;
    public float FireRate = 1.0f;

    private float fireTimer;

    private void Update()
    {
        // Accumulate delta time
        fireTimer += Time.deltaTime;

        // Wait until we reached our target time
        if (fireTimer > 1.0f / FireRate)
        {
            // Shoot here
            Shoot();

            // Reset the timer
            fireTimer = 0;
        }
    }

    GameObject AcquireTarget()
    {
        // Acquire all enemies
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Enemy");

        // Get the nearest
        return targets.OrderBy(t => Vector3.Distance(transform.position, t.transform.position)).FirstOrDefault();
    }

    void Shoot()
    {
        // Acquire target
        GameObject target = AcquireTarget();

        // Do not shoot if there are no targets
        if (target == null) return;

        // Instantiate the prefab
        GameObject projectile = Instantiate(Projectile);
        // At this point, projectile's Awake is already called
        projectile.transform.position = transform.position;

        // Set aiming direction. We have to explicitly access the Projectile component
        Projectile p = projectile.GetComponent<Projectile>();
        p.Direction = (target.transform.position - transform.position).normalized;
        p.tag = tag;
    }
}
