﻿using UnityEngine;
using System.Collections;

public class ChargingAI : MonoBehaviour
{
    public float Speed;
    public int Damage = 1;
    public GameObject Target;

    // Use this for initialization
    void Start()
    {
        // Acquire player
        Target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Target)
        {
            // Flip towards target
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.flipY = Target.transform.position.x < transform.position.x;

            // Follow target
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, Speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Check for opposing team
        if (tag != collision.tag)
        {
            Health health = collision.GetComponent<Health>();
            // Check if what we're hitting has health
            if (health)
            {
                // Apply damage
                health.TakeDamage(Damage);

                // Destroy the projectile here
                Destroy(gameObject);
            }
        }
    }
}
